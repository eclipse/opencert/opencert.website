---
date: 2018-06-01
title: Meet AMASS at EclipseCon France 2018
---

The AMASS project will participate again to the Open Research lab booth at EclipseCon France 2018 on June 13th-14th.

<!--more-->
[Register](https://www.eclipsecon.org/france2018/registration) and meet us at the conference.

_Article by **Gaël Blondelle**, Eclipse Foundation_

