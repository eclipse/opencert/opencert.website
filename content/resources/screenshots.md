---
date: 2018-12-13
title: Screenshots
---

This page shows some screenshots that illustrate the AMASS Open Platform in action.

<!--more-->


<p>
Starting the AMASS Tools Platform <br/>
<img src="../../images/screenshots/screenshot_00.png" class="img-responsive" alt="Starting the AMASS Tools Platform">
</p>

<p>
The big picture<br/>
<img src="../../images/screenshots/screenshot_01.png" class="img-responsive" alt="The big picture">
</p>
<br/><br/>
<p>
Architecture Modeling <br/>
<img src="../../images/screenshots/screenshot_02.png" class="img-responsive" alt="Architecture Modeling">
</p>
<br/><br/>
<p>
Process Activity Diagram <br/>
<img src="../../images/screenshots/screenshot_03.png" class="img-responsive" alt="Process Activity Diagram">
</p>
<br/><br/>
<p>
Process Model Diagram<br/>
<img src="../../images/screenshots/screenshot_04.png" class="img-responsive" alt="Process Model Diagram">
</p>