---
date: 2019-01-24
title: Considerations about open source and security
---
By Maria Teresa Delgado and Gaël Blondelle - Eclipse Foundation Europe GmbH

Security and openness are two orthogonal issues and the AMASS Open Tool Platform is certainly not a liability for the development of CPS
<!--more-->

[AMASS](http://www.amass-ecsel.eu) (Architecture-driven, Multi-concern and Seamless Assurance and Certification of Cyber-Physical Systems) is an EU-funded project that has created and consolidated a de-facto European Open Tool Platform, ecosystem, and community for assurance and certification of Cyber-Physical Systems (CPS). The tools provided by the AMASS open platform leverage both existing open source components, and new open source components that are published to the community, under an open collaborative model. The resulting open source ecosystem and community are managed as an [Eclipse project](https://www.polarsys.org/opencert/) hosted by the [Eclipse Foundation](https://www.eclipse.org/).
As AMASS is addressing a wide universe of application areas (i.e. automotive, railway, aerospace, space, and energy) while implementing an open collaboration model to develop its technology solutions, it is not surprising that the community would express its concern on the platform security aspects and its openness.
But no worries! As you will learn after reading this article, security and openness are two orthogonal issues and the AMASS Open Tool Platform is certainly not a liability for the development of CPS.

<p>
<img src="../../images/bernard-hermant-590572-unsplash_camera.png" class="img-responsive" alt="Close look @ camera">
</p>

## No direct relationship between Open source and security

[Open Source (OS)](https://opensource.org/osd-annotated) implies that source code is distributed under a license in which the copyright holder grants the users the power to access, modify and re-distribute the software to anyone and for any purpose. Nowadays, [OS components are the core building blocks of application software](https://resources.whitesourcesoftware.com/white-papers/the-complete-guide-on-open-source-security), providing developers with an ever-growing offer of off-the-shelf possibilities that they can use for assembling their products faster and more efficiently.

The OSS movement was not designed with security in mind, **OSS is all about open collaboration and open innovation**. However, the Linus’ law in Open Source states that: “given enough eyeballs, all bugs are shallow.” And the OS community do believe that opening their code up for inspection will increase protection against bugs — and often improve code trust. Nonetheless, [OSS community has evolved](http://resources.whitesourcesoftware.com/white-papers/the-complete-guide-on-open-source-security), and the vast majority of users are now downloading OS resources without actually reviewing the source code itself, meaning that the number of users is far greater than the number of eyeballs. Thus, having the source code available for scrutiny could be either a good or a bad thing, depending on the size of the community and the user perspective.

Others have studied the correlation between security and open source in a more structured way. [Schryen](https://aisel.aisnet.org/amcis2009/387) performed a thorough literature review on security aspects in open source vs. closed source software, concluding **that the discussion is often biased** depending on the preferences of development styles, and the lack of appropriate metrics, a common methodology and hard data. Schryen’s work analyses and compares published vulnerabilities (software bugs that can be used by attackers to gain access to a system or network are commonly referred to as vulnerabilities according to the [U.S. MITRE corporation](https://www.mitre.org/)) of a set of open and close source software packages, all of which are widely deployed. His investigation reveals that **no significant differences in the severity of vulnerabilities were found between open source and closed source software**.

## Closed source solutions are not necessarily more secure

Security wise, the main concern remains the surface of exposure of software code: all the different points where an unauthorized party could try to inject or extract data. The openness in OSS makes it easier for both the good and the bad guys to find vulnerabilities in the code, since it is available for anyone to review (and to fix!).

However, closed models implementing a **“security through obscurity” approach are not necessarily better**. Security is a holistic concept not only depending on the final result, but also linked to the creation and maintenance process, and **open source has the potential to be better than closed source** software in terms of security vulnerabilities being available for public scrutiny and fixes. But **[simply being open is not a guarantee of security](https://www.securityfocus.com/news/19)**; over the past years a few examples have made this clear for the OSS community: (a) the **[Heartbleed bug](http://heartbleed.com/)**, which put the spotlight on OpenSSL, the security toolkit used by many of the internet's largest sites, maintained primarily by [two men who have never met in person](https://www.buzzfeed.com/chrisstokelwalker/the-internet-is-being-protected-by-two-guys-named-st), (b) the **[Equifax breach](https://arstechnica.com/information-technology/2017/09/massive-equifax-breach-caused-by-failure-to-patch-two-month-old-bug/)** that exposed sensitive data for as many as 143 million U.S. consumers, accomplished by exploiting a web application vulnerability that had been [patched more than two months earlier](https://blogs.wsj.com/cio/2018/12/11/the-morning-download-house-equifax-report-cites-faulty-it-structure/), and \(c) the **[Apache Struts 2 flaw](https://threatpost.com/apache-struts-2-flaw-uncovered-more-critical-than-equifax-bug/136850/)** uncovered recently, which promises to be even more critical than the Equifax Bug, a remote code-execution vulnerability in the popular open-source framework for developing web applications in the Java programming language which could lead to full endpoint and network compromise.

On the other side, if you need to think about security breaches in proprietary solutions or closed source software, just think about the **Microsoft security breaches that we were never told about**, and you should be good to go, for example: that time when [Microsoft responded quietly](https://www.reuters.com/article/us-microsoft-cyber-insight/exclusive-microsoft-responded-quietly-after-detecting-secret-database-hack-in-2013-idUSKBN1CM0D0) to a detected secret database hack in 2013. Security in the 21st century has proven to have suffered enough [breaches](https://www.csoonline.com/article/2130877/data-breach/the-biggest-data-breaches-of-the-21st-century.html) both in the open and closed software worlds.

## Projection in the AMASS context

AMASS project partners and early adopters agree that security is crucial in all tools, systems and platforms, and of course AMASS results are no exception. However, we have made clear **that security and openness are two orthogonal issues** and we support the idea that **OSS is potentially better -security wise-** due to the public availability of source code.

In the context of the Eclipse ecosystem, some specific efforts are already in place to reinforce the security of the AMASS open platform. **Eclipse Development Process (EDP)** already covers the **traceability of the code published** in open source, checked by the Eclipse IP process that tracks the provenance of each contribution as well as the provenance of each dependency recursively. Also, EDP requires that the binaries are signed during the release process. 

Moreover, the AMASS open platform is supposed to be embedded in a larger environment, - either a proprietary product, or a specific deployment by a large organisation- where additional measures can be integrated to ensure the security of the platform. 

AMASS is about tools for assurance and certification processes that can be used in several domains to improve system efficiency, but AMASS is not a CPS core component, and thus the **AMASS tool platform by itself is not a liability for the development of CPS**. The AMASS open platform is deployed in the context of a global certification and assurance process that should consider the security risks related to the tools in order to effectively mitigate them.

