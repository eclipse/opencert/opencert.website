---
date: 2018-01-24
title: Downloads
---

---
## Current prototype
* OpenCert CHESS client - AMASS P2 prototype version for **Windows (64 Bit)** - [Zip-archive](https://drive.google.com/uc?id=1h2RHlQmFvf0gogI9IR0s4Wr1NlwuthSn&export=download)
* Read the [documentation](/opencert/resources/documentation/)
----

## Previous prototypes
## AMASS P1 prototype
* OpenCert CHESS client - AMASS P1 prototype version for **Windows (64 Bit)** - [Zip-archive](https://drive.google.com/uc?id=1sGewU6YKqmmbqac8vvraxghygEDNy2Ax&export=download)
* [User Manual] (AMASS_OpenPlatform_PrototypeP1_UserManual_24012018.pdf)
* [Developer Manual](AMASS_OpenPlatform_PrototypeP1_DeveloperGuide_24012018.pdf)

### Core prototype (version - 2017-03-07)

* OpenCert CHESS client - version for **Windows (64 Bit)** - [Zip-archive](https://drive.google.com/uc?id=1f4bIrXlZsSq-DwQB5v219A-vHGg2m3pV&export=download)
* User Manual: [OpenCert_UserManual_07062017.pdf](OpenCert_UserManual_07062017.pdf)
* Developer Manual: [OpenCert_DeveloperGuide_07062017.pdf](OpenCert_DeveloperGuide_07062017.pdf)